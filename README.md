# lp2pass - LastPass importer for pass

LastPass importer for [pass](http://www.passwordstore.org/). Reads CSV files exported from LastPass and imports them into pass.

# Usage

1. Export your LastPass Vault into a CSV file.
2. Fire up a terminal and run the script, passing the file you saved as an argument.

It should look something like this:

```
$ ./lp2pass path/to/passwords_file.csv
```

# Remarks

Based on [lastpass2pass.rb](https://github.com/asayers/lastpass-importer) by Alex Sayers <alex.sayers@gmail.com>.